import { ComponentType } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View } from '@tarojs/components'
import FuButton from "@/components/FuButton";
import CellTitle from "@/components/CellTitle";
import './index.scss'


class Index extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: 'Button按钮'
  }

  componentWillMount() { }

  componentWillReact() {
    console.log('componentWillReact')
  }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className='index'>
        <CellTitle title="普通按钮" />
        <View className="demo">
          <FuButton>Giao</FuButton>
          <FuButton type="primary" >Giao</FuButton>
          <FuButton type="info">Giao</FuButton>
          <FuButton type="warning">Giao</FuButton>
          <FuButton type="danger">Giao</FuButton>
        </View>
        <CellTitle title="镂空按钮" />
        <View className="demo">
          <FuButton type="primary" dashed>Giao</FuButton>
          <FuButton type="info" dashed>Giao</FuButton>
          <FuButton type="warning" dashed>Giao</FuButton>
          <FuButton type="danger" dashed>Giao</FuButton>
        </View>
        <CellTitle title="铺满" />
        <View style="padding:20px 0;background:#fff;">
          <FuButton type="primary" full>Giao</FuButton>
        </View>
        <CellTitle title="带icon的按钮" />
        <View className="demo">
          <FuButton icon="user1" type="info">Giao</FuButton>
          <FuButton icon="user1" dashed type="info">Giao</FuButton>
        </View>
      </View>
    )
  }
}

export default Index as ComponentType
