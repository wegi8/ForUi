import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import FuRadio from "@/components/FuRadio";
import CellTitle from "@/components/CellTitle";
import './index.scss'


class Index extends Component {

  state = {
    checkList: [
      {
        label: "北京",
      },
      {
        label: "上海",
      },
    ],
    checkListIcon: [
      {
        icon: "user1",
        label: "北京",
      },
      {
        icon: "user1",
        iconColor: "#f00",
        label: "上海",
      },
    ],
    checkListColor: [
      {
        icon: "user1",
        label: "北京",
      },
      {

        icon: "user1",
        iconColor: "#f00",
        label: "上海",
      },
    ],
    checkListDetail: [
      {
        icon: "user1",
        label: "北京",
        detail: "这是描述"
      },
      {

        icon: "user1",
        iconColor: "#f00",
        label: "上海",
        detail: "这是描述"
      },
    ],
    checkListDisabled: [
      {
        icon: "user1",
        label: "北京",
        detail: "这是描述",
        disabled: true
      },
      {

        icon: "user1",
        iconColor: "#f00",
        label: "上海",
        detail: "这是描述"
      },
      {

        icon: "user1",
        iconColor: "#f00",
        label: "深圳",
        detail: "这是描述"
      },
    ],
    checkListLine: [
      {
        icon: "user1",
        label: "北京",
        detail: "这是描述",
        disabled: true
      },
      {

        icon: "user1",
        iconColor: "#f00",
        label: "上海",
        detail: "这是描述"
      },
      {

        icon: "user1",
        iconColor: "#f00",
        label: "深圳",
        detail: "这是描述"
      },
    ]
  }

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const {
      checkList,
      checkListIcon,
      checkListColor,
      checkListDetail,
      checkListDisabled
    } = this.state;
    return (
      <View className='index'>
        <CellTitle title="普通Radio组" />
        <View className="demo">
          <FuRadio onChange={(val) => {
            Taro.showToast({
              title: "当前选中" + val
            })
          }} checkList={checkList} />
        </View>
        <CellTitle title="带icon的Radio组" />
        <View className="demo">
          <FuRadio checkList={checkListIcon} />
        </View>
        <CellTitle title="自定义选中颜色的Radio组" />
        <View className="demo">
          <FuRadio color="#6178fc" checkList={checkListColor} />
        </View>
        <CellTitle title="带描述的Radio组" />
        <View className="demo">
          <FuRadio color="#6178fc" checkList={checkListDetail} />
        </View>
        <CellTitle title="禁用的Radio组" />
        <View className="demo">
          <FuRadio color="#6178fc" checkList={checkListDisabled} />
        </View>
        <CellTitle title="行内的Radio组" />
        <View className="demo">
          <FuRadio line color="#6178fc" checkList={checkListDisabled} />
        </View>
        <CellTitle title="不同方向的行内Radio组" />
        <View className="demo">
          <FuRadio line lineDirection="left" checkList={checkListDisabled} />
        </View>
      </View>
    )
  }
}

export default Index as ComponentType
