import Taro, { Component, Config } from '@tarojs/taro'
import { View } from '@tarojs/components'
import CellTitle from "@/components/CellTitle";
import FuCell from "@/components/FuCell";
import FuSwipeCell from "@/components/FuSwipeCell";
import './index.scss'

class Index extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */


  state = {
    open: true
  }
  config: Config = {
    navigationBarTitleText: 'SwipeCell滑块单元格',
  }

  componentWillMount() { }

  componentWillReact() {
    console.log('componentWillReact')
  }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { open } = this.state
    return (
      <View className='index'>
        <CellTitle title="普通滑块单元格" />
        <FuSwipeCell
          rightWidth={240}
          renderRight={
            <View style={{ display: "flex", height: "100%" }}>
              <View
                className="del"
                onClick={() => {
                  Taro.showToast({
                    title: "成功111"
                  })
                }}>删除</View>
              <View
                className="del"
                style={{ backgroundColor: "#cfcfcf" }}
                onClick={() => {
                  Taro.showToast({
                    title: "成功222"
                  })
                }}>加入收藏</View>
            </View>
          }
        >
          <FuCell border arrow title="123" />
        </FuSwipeCell>
        <CellTitle title="默认打开的单元格" />
        <FuSwipeCell
          rightWidth={120}
          defalutOpen={open}
          renderRight={
            <View style={{ display: "flex", height: "100%" }} className="del" onClick={() => {
              Taro.showToast({
                title: "成功"
              })
            }}>侧滑</View>
          }
        >
          <FuCell border arrow title="123" />
        </FuSwipeCell>
      </View>
    )
  }
}

export default Index
