export default [
"address-book",

"adjust",

"address-card",

"align-center",

"air-freshener",

"align-justify",

"align-left",

"align-right",

"ambulance",

"american-sign-langua",

"anchor",

"allergies",

"angle-double-right",

"angle-double-left",

"angle-double-up",

"angle-double-down",

"angle-down",

"angle-left",

"angle-up",

"angry",

"apple-alt",

"archive",

"archway",

"arrow-alt-circle-dow",

"angle-right",

"arrow-alt-circle-lef",

"arrow-alt-circle-rig",

"arrow-alt-circle-up",

"arrow-circle-down",

"arrow-circle-left",

"arrow-circle-right",

"arrow-circle-up",

"arrow-down",

"arrow-left",

"arrow-right",

"arrows-alt",

"arrows-alt-h",

"arrows-alt-v",

"arrow-up",

"assistive-listening-",
"asterisk",
"at",

"atlas",

"atom",

"award",

"backspace",

"backward",

"audio-description",

"balance-scale",

"ban",

"band-aid",

"barcode",

"bars",

"baseball-ball",

"bell",

"bell-slash",

"bezier-curve",

"binoculars",

"bicycle",

"blender",

"birthday-cake",

"blind",

"bold",

"bolt",

"bomb",

"bone",

"bong",

"bookmark",

"book-open",

"book-reader",

"bowling-ball",

"boxes",

"box-open",

"box",

"braille",

"brain",

"briefcase",

"caret-right",

"caret-down",

"caret-left",

"caret-square-left",

"caret-square-right",

"caret-square-up",

"caret-up",

"check",

"check-square",

"chevron-down",

"chevron-left",

"chevron-right",

"chevron-up",

"cloud-download-alt",

"cloud-upload-alt",

"cog",

"comment-dots",

"cookie",

"cookie-bite",

"copy",

"copyright",

"couch",

"crosshairs",

"desktop",

"dove",

"ellipsis-h",

"ellipsis-v",

"envelope",

"envelope-open",

"envelope-square",

"equals",

"expand",

"expand-arrows-alt",

"exclamation-circle",

"fingerprint",

"genderless",

"home",

"key",

"list",

"list-ul",

"list-ol",

"male",

"map-marked",

"map-marked-alt",

"play-circle",

"redo",

"redo-alt",

"rocket",

"search",

"search-minus",

"search-plus",

"smoking",

"smoking-ban",

"sort-alpha-down",

"sort-amount-down",

"sort-amount-up",

"sort-down",

"stop-circle",

"sync-alt",

"toggle-off",

"toggle-on",

"upload",

"user",

"user-alt",

"user-alt-slash",

"user-astronaut",

"user-check",

"user-circle",

"user-clock",

"user-cog",

"user-edit",

"user-friends",

"user-graduate",

"user-lock",

"user-md",

"user-minus",

"user-ninja",

"wifi",

"app-store",

"github",

"github-alt",

"google-plus-g",

"js-square",

"stack-overflow",

"telegram",

"telegram-plane",

"twitter",

"twitter-square",

"vuejs",

"weibo",

"weixin",

"windows",

"wolf-pack-battalion",

"wordpress",

"xbox",

"youtube-square",

"zhihu",

"arrow-alt-circle-lef1",

"arrow-alt-circle-rig1",

"arrow-alt-circle-up1",

"bookmark1",

"calendar",

"calendar-minus",

"calendar-alt",

"calendar-check",

"calendar-plus",

"calendar-times",

"check-circle",

"circle",

"check-square1",

"clipboard",

"clock",

"clone",

"compass",

"copy1",

"copyright1",

"credit-card",

"dizzy",

"dot-circle",

"edit",

"envelope1",

"envelope-open1",

"eye",

"file-pdf",

"file-powerpoint",

"file-video",

"file-word",

"flushed",

"flag",

"frown",

"sun",

"trash-alt",

"user1",

"user-circle1",

]