import { ComponentType } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View } from '@tarojs/components'
import FuIcon from '@/components/FuIcon'
import CellTitle from "@/components/CellTitle";
// import iconList from "./IconJson"
import './index.scss'

class Index extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: 'Icon图标'
  }

  constructor() {
    super()
  }

  componentWillMount() { }

  componentWillReact() {
    console.log('componentWillReact')
  }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className='index'>
        <CellTitle title="ICON" />
        <View className="demo">
          <FuIcon name='trash-alt'></FuIcon>
          <FuIcon name='sun'></FuIcon>
          <FuIcon name='user1'></FuIcon>
        </View>
        <CellTitle title="更改ICON颜色" />
        <View className="demo">
          <FuIcon name='trash-alt' color="#f44"></FuIcon>
          <FuIcon name='sun' color="#f93"></FuIcon>
          <FuIcon name='user1' color="#f66"></FuIcon>
        </View>
        <CellTitle title="更改ICON大小" />
        <View className="demo">
          <FuIcon size="30px" name='trash-alt' color="#f44"></FuIcon>
          <FuIcon size="30px" name='sun' color="#f93"></FuIcon>
          <FuIcon size="30px" name='user1' color="#f66"></FuIcon>
        </View>
      </View>
    )
  }
}

export default Index as ComponentType
