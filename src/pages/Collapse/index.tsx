import { ComponentType } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View } from '@tarojs/components'
import FuCollapse from "@/components/FuCollapse";
import CellTitle from "@/components/CellTitle";
import './index.scss'


class Index extends Component {

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className='index'>
        <CellTitle title="普通折叠面板" />
        <View className="demo">
          <FuCollapse border title="折叠面板">
            <View>
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
            </View>
          </FuCollapse>
        </View>
        <CellTitle title="带icon的折叠面板" />
        <View className="demo">
          <FuCollapse icon="user1" border title="折叠面板">
            <View>
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
            </View>
          </FuCollapse>
          <FuCollapse icon="user1" iconColor="#f00" border title="折叠面板">
            <View>
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
            </View>
          </FuCollapse>
          <FuCollapse icon="user1" iconColor="#f00" iconSize="30px" border title="折叠面板">
            <View>
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
            </View>
          </FuCollapse>
        </View>
        <CellTitle title="不带箭头的折叠面板" />
        <View className="demo">
          <FuCollapse icon="user1" arrow={false} border title="折叠面板">
            <View>
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
            </View>
          </FuCollapse>
        </View>
        <CellTitle title="默认展开的折叠面板" />
        <View className="demo">
          <FuCollapse icon="user1" open arrow={false} border title="折叠面板">
            <View>
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
              这是一个折叠面板
            </View>
          </FuCollapse>
        </View>
      </View>
    )
  }
}

export default Index as ComponentType
