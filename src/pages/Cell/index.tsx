import { ComponentType } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View } from '@tarojs/components'
import CellTitle from "@/components/CellTitle";
import FuCell from "@/components/FuCell";
import './index.scss'

class Index extends Component {

  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: 'Cell单元格',
  }

  componentWillMount() { }

  componentWillReact() {
    console.log('componentWillReact')
  }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className='index'>
        <CellTitle title="普通单元格" />
        <FuCell title='单元格' value='内容' />
        <CellTitle title="详情单元格" />
        <FuCell title='单元格' label='详情信息' />
        <FuCell title='单元格' value='内容' label='详情信息' />
        <CellTitle title="带按钮的单元格" />
        <FuCell title='单元格' value='内容' label='详情信息' arrow />
        <CellTitle title="有边框的单元格" />
        <FuCell title='单元格' value='内容' label='详情信息' border arrow />
        <CellTitle title="带ICON的单元格" />
        <FuCell icon='user1' title='单元格' value='内容' border arrow />
        <FuCell icon='user1' iconColor="#f44" title='单元格' value='内容' border arrow />
        <FuCell icon='user1' iconColor="#f44" iconSize="30px" title='单元格' label='详情信息' value='内容' border arrow />
      </View>
    )
  }
}

export default Index as ComponentType
