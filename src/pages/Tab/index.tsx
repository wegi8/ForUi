import { ComponentType } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View } from '@tarojs/components'
import FuTab from "@/components/FuTab";
import CellTitle from "@/components/CellTitle";
import './index.scss'


const list = [
  { title: "Tab1" },
  { title: "Tab2" },
  { title: "Tab3" },
  { title: "Tab4" },
]
const list2 = [
  { title: "Tab1" },
  { title: "Tab2" },
  { title: "Tab3" },
  { title: "Tab4" },
  { title: "Tab5" },
  { title: "Tab6" },
  { title: "Tab7" },
  { title: "Tab8" },
  { title: "Tab9" },
]

const list3 = [
  { title: "Tab1", icon:"user1" },
  { title: "Tab2", icon:"archway" },
  { title: "Tab3", icon:"atlas" },
  { title: "Tab4", icon:"adjust" },
]

class Index extends Component {

  config: Config = {
    navigationBarTitleText: 'Tab标签页'
  }

  componentWillMount() { }

  componentWillReact() {
    console.log('componentWillReact')
  }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    return (
      <View className='index'>
        <CellTitle title="普通Tab" />
        <View className="demo">
          <FuTab list={list} />
        </View>
        <CellTitle title="自定义颜色的Tab" />
        <View className="demo">
          <FuTab list={list} itemStyle={{
            color: "#a449f3",
            borderColor: "#a449f3"
          }} />
        </View>
        <CellTitle title="可以滚动的Tab" />
        <View className="demo">
          <FuTab scroll list={list2} itemStyle={{
            color: "#a449f3",
            borderColor: "#a449f3"
          }} />
        </View>
        <CellTitle title="选中部分滚动到前列的Tab" />
        <View className="demo">
          <FuTab scrollCenter scroll list={list2} itemStyle={{
            color: "#a449f3",
            borderColor: "#a449f3"
          }} />
        </View>
        <CellTitle title="带icon的Tab" />
        <View className="demo">
          <FuTab list={list3} itemStyle={{
            color: "#a449f3",
            borderColor: "#a449f3"
          }} />
        </View>
      </View>
    )
  }
}

export default Index as ComponentType
