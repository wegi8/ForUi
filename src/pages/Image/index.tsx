import { ComponentType } from 'react'
import Taro, { Component, Config } from '@tarojs/taro'
import { View } from '@tarojs/components'
import FuImage from "@/components/FuImage";
import CellTitle from "@/components/CellTitle";
import './index.scss'

class Index extends Component {

  state = {
    url: "https://mp-yys-1255362963.cos.ap-chengdu.myqcloud.com/head/330.jpg",
    url2: "https://img.yzcdn.cn/vant/cat.jpeg",
    urlList: [
      { src: "https://img.yzcdn.cn/vant/cat.jpeg" },
      { src: "https://img.yzcdn.cn/vant/cat.jpeg" },
      { src: "https://mp-yys-1255362963.cos.ap-chengdu.myqcloud.com/head/330.jpg" },
      { src: "https://mp-yys-1255362963.cos.ap-chengdu.myqcloud.com/head/330.jpg" },
    ]
  }

  config: Config = {
    navigationBarTitleText: 'Image图片'
  }

  componentWillMount() { }

  componentWillReact() {
    console.log('componentWillReact')
  }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { url, url2, urlList } = this.state;
    return (
      <View className='index'>
        <CellTitle title="普通图片" />
        <View className="demo">
          <FuImage src={url} />
        </View>
        <CellTitle title="大小不同的图片" />
        <View className="demo">
          <FuImage size="small" src={url} />
          <FuImage size="normal" src={url} />
          <FuImage size="large" fit="widthFix" src={url} />
        </View>
        <CellTitle title="自定义尺寸的图片" />
        <View className="demo">
          <FuImage width={200} height={100} src={url} />
        </View>
        <CellTitle title="填充图片" />
        <View className="fit">
          <View>
            <FuImage fit="widthFix" src={url2} />
            <View>widthFix</View>
          </View>
          <View>
            <FuImage fit="heightFix" src={url2} />
            <View>heightFix</View>
          </View>
          <View>
            <FuImage fit="fill" src={url2} />
            <View>fill</View>
          </View>
          <View>
            <FuImage fit="cover" src={url2} />
            <View>cover</View>
          </View>
          <View>
            <FuImage fit="contain" src={url2} />
            <View>contain</View>
          </View>
        </View >
        <CellTitle title="圆形图片" />
        <View className="fit">
          <View>
            <FuImage round fit="widthFix" src={url2} />
            <View>widthFix</View>
          </View>
          <View>
            <FuImage round fit="heightFix" src={url2} />
            <View>heightFix</View>
          </View>
          <View>
            <FuImage round fit="fill" src={url2} />
            <View>fill</View>
          </View>
          <View>
            <FuImage round fit="cover" src={url2} />
            <View>cover</View>
          </View>
          <View>
            <FuImage round fit="contain" src={url2} />
            <View>contain</View>
          </View>
        </View>
        <CellTitle title="图片组" />
        <View className="group">
          {
            urlList.map((item: any) => {
              return <FuImage group round fit="cover" {...item} />
            })
          }
        </View>
        <CellTitle title="文字图片" />
        <View className="demo">
          <FuImage fit="cover" text="为了" />
          <FuImage round fit="cover" text="你" />
        </View>
        {/* <CellTitle title="openData图片(授权后可查看)" /> */}
        {/* <View className="demo"> */}
          {/* <FuImage openData={{ type: "userAvatarUrl" }} /> */}
          {/* <FuImage round openData={{ type: "userAvatarUrl" }} /> */}
        {/* </View> */}
      </View>
    )
  }
}

export default Index as ComponentType
