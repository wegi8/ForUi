import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Button } from '@tarojs/components'
import FuInput from "@/components/FuInput";
import CellTitle from "@/components/CellTitle";
import './index.scss'


class Index extends Component {

  state = {
    value: "123"
  }

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { value } = this.state;
    return (
      <View className='index'>
        <CellTitle title="普通input" />
        <View className="demo">
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
          ></FuInput>
        </View>
        <CellTitle title="事件触发input" />
        <View className="demo">
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
            onFocus={() => Taro.showToast({ title: "触发了焦点事件" })}
            onBlur={() => Taro.showToast({ title: "触发了失去焦点事件" })}
          ></FuInput>
        </View>
        <CellTitle title="设置长度为3的input" />
        <View className="demo">
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
            maxLength={3}
          ></FuInput>
        </View>
        <CellTitle title="被禁用的input" />
        <View className="demo">
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
            enable
          ></FuInput>
        </View>
        <CellTitle title="带自定义icon的input" />
        <View className="demo">
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
            icon="user1"
          ></FuInput>
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
            icon="user1"
            iconColor="#f00"
            iconSize="30px"
          ></FuInput>
        </View>
        <CellTitle title="带清除按钮的input" />
        <View className="demo">
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
            clear
          ></FuInput>
        </View>
        <CellTitle title="自定义右边栏的icon" />
        <View className="demo">
          <FuInput
            title="用户名"
            placeholder="这是一个普通的input"
          >
            <Button>按钮</Button>
          </FuInput>
        </View>
        <CellTitle title="自定义输入项的input" />
        <View className="demo">
          <FuInput
            type="number"
            title="用户名"
            placeholder="这是一个数字input"
          ></FuInput>
          <FuInput
            password
            title="用户名"
            placeholder="这是一个密码input"
          ></FuInput>
        </View>
      </View>
    )
  }
}

export default Index as ComponentType
