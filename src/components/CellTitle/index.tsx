import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { Button, View } from '@tarojs/components'
import FuIcon from '@/components/FuIcon'
import './index.scss'

/**
 *@param {title} 名称
 *@param {value} 内容
 *@param {label} 详情信息
 *@param {arrow} 是否显示箭头
 *@param {border} 是否包含下边框
 *@param {icon} icon内容
 *@param {iconColor} icon颜色
 *@param {iconSize} icon大小
 *@param {onClick} 点击事件
 */
type CellTitles = {
  title: string
}


class CellTitle extends Component<CellTitles> {
  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { title } = this.props;
    return (
      <View className="CellTitles">
        {title}
      </View>
    )
  }
}

export default CellTitle as ComponentType<CellTitles>
