import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import FuIcon from '@/components/FuIcon'
import { View } from '@tarojs/components'
import './index.scss'


type FuCollapses = {
  title: string,
  value?: string,
  label?: string,
  open?: boolean,
  border?: boolean,
  disabled?: boolean,
  arrow?: boolean,
  customStyle?: Object,
  contentStyle?: Object,
  icon?: string,
  iconColor?: string,
  iconSize?: string,
  onChang?: () => void;
}


class FuCollapse extends Component<FuCollapses> {

  state = {
    open: this.props.open || false
  }

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  borderShow() {
    const { border } = this.props
    const str = "fu-collapse-base " + (border ? "fu-collapse-border" : "");
    return str
  }

  changState() {
    const { open } = this.state;
    this.setState({ open: !open });
  }

  render() {
    const { open } = this.state;
    const { title, value, label, arrow = true, icon, iconColor, iconSize, onChang, customStyle = {} } = this.props;
    return (
      <View className="fu-collapse">
        <View
          onClick={() => { this.setState({ open: !open }); onChang && onChang(); }}
          style={customStyle}
          className={this.borderShow()}
        >
          <View className="fu-collapse-base-title">
            {icon ?
              <View className="fu-collapse-base-title-icon">
                <FuIcon name={icon || ""} color={iconColor} size={iconSize} ></FuIcon>
              </View> : null
            }
            <View>
              {title}
              <View className="fu-collapse-base-title-label">{label}</View>
            </View>
          </View>
          <View className="fu-collapse-base-value">
            {value}
            {
              arrow ?
                <View className={"fu-collapse-base-value-arrowicon " +
                  (open ? "fu-collapse-base-value-arrowicon-open" :
                    "fu-collapse-base-value-arrowicon-close")}>
                </View> : null}
          </View>
        </View>
        <View className={"fu-collapse-content " +
          (open ? "fu-collapse-content-open" :
            "fu-collapse-content-close")}>
          {this.props.children}
        </View>
      </View>
    )
  }
}

export default FuCollapse as ComponentType<FuCollapses>
