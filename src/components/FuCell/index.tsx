import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'
import FuIcon from '@/components/FuIcon'
import './index.scss'

/**
 *@param {title} 名称
 *@param {value} 内容
 *@param {label} 详情信息
 *@param {arrow} 是否显示箭头
 *@param {border} 是否包含下边框
 *@param {icon} icon内容
 *@param {iconColor} icon颜色
 *@param {iconSize} icon大小
 *@param {onClick} 点击事件
 */
type FuCellType = {
  title: string,
  value?: string,
  label?: string,
  arrow?: boolean,
  border?: boolean,
  icon?: string,
  iconColor?: string,
  iconSize?: string,
  onClick?: () => void
}


class FuCell extends Component<FuCellType> {
  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  borderShow() {
    const { border } = this.props
    const str = "fu-cell " + (border ? "fu-cell-border" : "");
    return str
  }

  render() {
    const { title, value, label, arrow, icon, iconColor, iconSize, onClick } = this.props;
    return (
      <View onClick={
        ()=> onClick && onClick()
      } className={this.borderShow()}>
        <View className="fu-cell-title">
          {icon ?
            <View className="fu-cell-title-icon">
              <FuIcon name={icon || ""} color={iconColor} size={iconSize} ></FuIcon>
            </View> : null
          }
          <View>
            {title}
            <View className="fu-cell-title-label">{label}</View>
          </View>
        </View>
        <View className="fu-cell-value">
          {value}
          {
            arrow ? <View className="fu-cell-value-arrowicon"></View> : null
          }

        </View>
      </View>
    )
  }
}

export default FuCell as ComponentType<FuCellType>
