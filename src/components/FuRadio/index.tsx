import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Radio, RadioGroup } from '@tarojs/components'
import FuIcon from '@/components/FuIcon'
import './index.scss'

/**
 *@param {check} 默认选中
 *@param {label} 显示
 *@param {value} 实际内容
 *@param {line} 是否为行内样式
 *@param {icon} icon内容
 *@param {iconColor} icon颜色
 *@param {iconSize} icon大小
 *@param {onChange} 点击事件
 */
type FuRadioItem = {
  checked?: boolean,
  value?: string,
  label: string,
  icon?: string,
  iconColor?: string,
  iconSize?: string,
  detail?: string,
  disabled?: boolean
}


type FuRadios = {
  checkList: Array<FuRadioItem>
  line?: boolean,
  lineDirection?: "left" | "right"
  color?: string,
  onChange?: (val?: any) => void
}


class FuRadio extends Component<FuRadios> {

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }


  render() {
    const { checkList = [], color, line, lineDirection = "right", onChange } = this.props;
    return (
      <View className="fu-radios">
        {
          line ? <RadioGroup onChange={(e) => onChange && onChange(e.target.value)} className="fu-radios-line"> {
            checkList.map(item => {
              return <View className="fu-radios-line-item">
                {lineDirection == "left" && <Radio disabled={item.disabled} color={color} value={item.value || item.label} checked={item.checked}></Radio>}
                <View className="fu-radios-line-item-label">
                  {item.label}
                </View>
                {lineDirection == "right" && <Radio disabled={item.disabled} color={color} value={item.value || item.label} checked={item.checked}></Radio>}
              </View>
            })
          }
          </RadioGroup>
            :
            <RadioGroup onChange={(e) => onChange && onChange(e.target.value)} className="fu-radios-block">
              {checkList.map(item => {
                return <View className="fu-radios-block-item">
                  {item.icon &&
                    <View className="fu-radios-block-item-icon">
                      <FuIcon name={item.icon || ""} color={item.iconColor} size={item.iconSize} ></FuIcon>
                    </View>
                  }
                  <View className="fu-radios-block-item-label">
                    {item.label}
                    {item.detail && <View className="fu-radios-block-item-detail">
                      {item.detail}
                    </View>}
                  </View>
                  <Radio disabled={item.disabled} color={color} value={item.value || item.label} checked={item.checked}></Radio>
                </View>
              })}
            </RadioGroup>
        }

      </View>
    )
  }
}

export default FuRadio as ComponentType<FuRadios>
