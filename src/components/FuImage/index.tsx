import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { View, Image } from '@tarojs/components'
import './index.scss'
// import { OpenDataProps } from '@tarojs/components/types/OpenData'

/**
 *@param {width} 名称
 *@param {height} 内容
 *@param {src} 详情信息
 *@param {size} 是否显示箭头
 *@param {lazyLoad} 是否包含下边框
 *@param {fit} icon内容
 *@param {round} icon颜色
 */
type FuImages = {
  width?: number,
  height?: number,
  src?: string,
  size?: "small" | "normal" | "large" | undefined,
  lazyLoad?: boolean,
  fit?: "contain" | "cover" | "fill" | "widthFix" | "heightFix" | "none" | undefined,
  round?: boolean,
  group?: boolean,
  text?: string,
  // openData?: OpenDataProps
}

var FIT_MODE_MAP = {
  none: 'center',
  fill: 'scaleToFill',
  cover: 'aspectFill',
  contain: 'aspectFit',
  widthFix: 'widthFix',
  heightFix: 'heightFix',
};

class FuImage extends Component<FuImages> {

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  setStyle = () => {
    const { width, height } = this.props;
    return {
      width: width + "px",
      height: height + "px"
    }
  }

  render() {
    const { size = "normal", round, group, text, fit, src, lazyLoad } = this.props;
    // const { size = "normal",  } = this.props;
    return (
      <View className={"fu-image " + "fu-image-" + size + (round ? " fu-image-round" : "") + (group ? " fu-image-group" : "")}>
        {
          src ? <Image
            src={src}
            mode={FIT_MODE_MAP[fit || "aspectFit"]}
            lazy-load={lazyLoad}
            className={`fu-image-${size}`}
            show-menu-by-longpress="{{ showMenuByLongpress }}"
            onLoad={(e) => {}}
            onError={(e) => {}}
            style={this.setStyle()}
          /> : <View className={`fu-image-${size} fu-image-text`}>{text ? (text.length > 1 ? text[0] : text) : "无"}</View>
        }
      </View>
    )
  }
}

export default FuImage as ComponentType<FuImages>
