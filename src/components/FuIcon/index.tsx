import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { Text } from '@tarojs/components'
import "./icon/iconfont.css"
import './index.scss'


type FuIcons = {
  name: string,
  size?: string,
  color?: string,
  customStyle?: Object,
  type?: string,
  onClick?: () => void
}


class FuIcon extends Component<FuIcons> {

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const { type = "defalut", name, size, color, customStyle, onClick } = this.props
    return (
      <Text
        onClick={() => onClick && onClick()}
        className={"iconfont icon-" + name + " fu-icon" + type}
        style={{
          fontSize: size,
          color: color,
          ...customStyle
        }}
      >
      </Text>
    )
  }
}

export default FuIcon as ComponentType<FuIcons>
