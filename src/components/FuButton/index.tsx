import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import { Button } from '@tarojs/components'
import FuIcon from '@/components/FuIcon'
import './index.scss'

/**
 *@param {title} 名称
 *@param {value} 内容
 *@param {label} 详情信息
 *@param {arrow} 是否显示箭头
 *@param {border} 是否包含下边框
 *@param {icon} icon内容
 *@param {iconColor} icon颜色
 *@param {iconSize} icon大小
 *@param {onClick} 点击事件
 */
type FuButtons = {
  type?: string,
  dashed?: boolean,
  full?: boolean,
  openType?: "contact" | "contactShare" | "share" | "getRealnameAuthInfo" | "getAuthorize" | "getPhoneNumber" | "getUserInfo" | "lifestyle" | "launchApp" | "openSetting" | "feedback" | undefined,
  lang?: "en" | "zh_CN" | "zh_TW" | undefined,
  disabled?: boolean,
  sendMessageTitle?: string | undefined,
  sendMessagePath?: string | undefined,
  sendMessageImg?: string | undefined,
  showMessageCard?: boolean | undefined,
  onClick?: (event) => void | undefined,
  onGetUserInfo?: (event) => void | undefined,
  onContact?: (event) => void | undefined,
  onGetPhoneNumber?: (event) => void | undefined,
  onError?: (event) => void | undefined,
  onOpenSetting?: (event) => void | undefined,
  children?: any,
  icon?: string,
}


class FuButton extends Component<FuButtons> {
  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  private onBtnClick = (event: any): void => {
    if (!this.props.disabled) {
      this.props.onClick && this.props.onClick(event)
    }
  }

  private onBtnGetUserInfo(event: any): void {
    this.props.onGetUserInfo && this.props.onGetUserInfo(event)
  }

  private onBtnContact(event: any): void {
    this.props.onContact && this.props.onContact(event)
  }

  private onBtnGetPhoneNumber(event: any): void {
    this.props.onGetPhoneNumber && this.props.onGetPhoneNumber(event)
  }

  private onBtnError(event: any): void {
    this.props.onError && this.props.onError(event)
  }

  private onBtnOpenSetting(event: any): void {
    this.props.onOpenSetting && this.props.onOpenSetting(event)
  }

  private buttonClass(): string {
    const { type, dashed, full } = this.props;
    const str = "fu-button " + (type ? ' fu-button-' + type : '') + (dashed ? ' fu-button-' + type + '-dashed  ' : '  ') + (full ? ' full' : ' ')
    return str
  }
  render() {
    const {
      type,
      icon,
      openType,
      lang,
      disabled,
      sendMessageTitle,
      sendMessagePath,
      sendMessageImg,
      showMessageCard,
    } = this.props;
    return (
      <Button
        className={this.buttonClass()}
        openType={openType}
        lang={lang}
        sendMessageTitle={sendMessageTitle}
        sendMessagePath={sendMessagePath}
        sendMessageImg={sendMessageImg}
        showMessageCard={showMessageCard}
        onClick={this.onBtnClick.bind(this)}
        onGetUserInfo={this.onBtnGetUserInfo.bind(this)}
        onGetPhoneNumber={this.onBtnGetPhoneNumber.bind(this)}
        onOpenSetting={this.onBtnOpenSetting.bind(this)}
        onError={this.onBtnError.bind(this)}
        onContact={this.onBtnContact.bind(this)}
        disabled={disabled}
      >
        {icon ?
          <FuIcon type={type} name={icon || ""} ></FuIcon>
          : null
        }
        {this.props.children}
      </Button>
    )
  }
}

export default FuButton as ComponentType<FuButtons>
