import { ComponentType } from 'react'
import Taro, { Component } from '@tarojs/taro'
import FuIcon from '@/components/FuIcon'
import { View, Input } from '@tarojs/components'
import './index.scss'


type FuInputs = {
  title?: string,
  type?: "text" | "number" | "idcard" | "digit",
  icon?: string,
  iconColor?: string,
  iconSize?: string,
  placeholder?: string,
  value?: string,
  password?: boolean,
  clear?: boolean,
  maxLength?: number,
  placeholderStyle?: string,
  confirmType?: string,
  enable?: boolean,
  onChange?: (value?) => void;
  onFocus?: () => void;
  onBlur?: () => void;
}


class FuInput extends Component<FuInputs> {

  state = {
    value: this.props.value || ""
  }

  componentWillMount() { }

  componentWillReact() { }

  componentDidMount() { }

  componentWillUnmount() { }

  componentDidShow() { }

  componentDidHide() { }

  render() {
    const {
      title,
      type = "text",
      placeholder,
      placeholderStyle,
      confirmType,
      enable = false,
      clear,
      icon,
      iconColor,
      iconSize,
      password,
      onChange,
      onFocus,
      onBlur
    } = this.props;
    let { maxLength = 9999 } = this.props
    let { value = "" } = this.state
    if (enable) maxLength = 0;
    return (
      <View className="fu-input">
        {icon &&
          <View className={"fu-input-title"}>
            <FuIcon name={icon || ""} color={iconColor} size={iconSize} ></FuIcon>
          </View>
        }
        {
          title && <View className={"fu-input-title " + (enable ? "fu-input-title-enable" : "")}>{title}</View>
        }
        {
          enable ? <View className="fu-input-title-enable"> {value || placeholder} </View> :
            <Input
              password={password}
              className="fu-input-content"
              type={type}
              placeholder={placeholder}
              value={value}
              maxLength={maxLength}
              placeholderStyle={placeholderStyle}
              confirmType={confirmType}
              onInput={(e) => {
                this.setState({ value: e.target.value });
                onChange && onChange(e.target.value)
              }}
              onFocus={() => onFocus && onFocus()}
              onBlur={() => onBlur && onBlur()}
            />
        }
        {
          clear && <FuIcon onClick={() => {
            this.setState({ value: "" });
            onChange && onChange("");
          }} name="times-circle"></FuIcon>
        }
        {
          !clear && this.props.children
        }
      </View>
    )
  }
}

export default FuInput as ComponentType<FuInputs>
